package com.example.demo.hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.TestCase.assertNotNull;


/**
 * Created by rajeev.s on 5/22/18.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class ApplicationTest
{

    @Autowired
    BeanWithoutDependency beanWithoutDependency;

    @Autowired
    MessagePrinter messagePrinter;

    @Test
    public void beanWithoutDependencyShouldNotBeNull()
    {
        assertNotNull(beanWithoutDependency);

//        beanWithoutDependency.print();

        messagePrinter.printMessage();
    }
}
