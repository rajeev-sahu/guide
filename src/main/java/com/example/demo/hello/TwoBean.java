package com.example.demo.hello;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by rajeev.s on 5/21/18.
 */
@Component
public class TwoBean
{
    int age;
    String name;

    @Autowired
    public void of(@Value("${age}") final String age, @Value("${name}") final String name)
    {
        System.out.println("Called....");
        this.name = Objects.requireNonNull(name, "Name can't empty");
        this.age = Objects.requireNonNull(Integer.parseInt(age), "Incorrect Age");
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("name", name)
                .append("age", age)
                .toString();
    }
}
