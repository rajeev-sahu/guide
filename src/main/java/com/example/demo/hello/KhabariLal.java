package com.example.demo.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/**
 * Created by rajeev.s on 5/20/18.
 */
@Component
public class KhabariLal
{
    @Autowired(required = true)
    private final MessageService messageService2;


    public KhabariLal(MessageService messageService2)
    {
        this.messageService2 = messageService2;
    }

    public void printMessage()
    {
        System.out.println("KhabariLal.messageService = " + messageService2.getMessage());
    }

    @Autowired
    public void printMessageWithOwnMessageService(MessageService messageServiceNew)
    {
        System.out.println(MessageFormat.format("KhabariLal:BHai {0}",
                                                messageServiceNew.getMessage()));
    }
}
