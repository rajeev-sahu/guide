package com.example.demo.hello;

import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/21/18.
 */
@Component
public class OneBean
{
    private TwoBean twoBean;

    private OneBean(final TwoBean twoBean)
    {
        System.out.println("OneBean.CONSTRUCTOR");
        this.twoBean = twoBean;
    }

    public static OneBean of(final TwoBean twoBean)
    {
        System.out.println("OneBean.of");
        return new OneBean(twoBean);
    }

    public void log()
    {
        System.out.println(twoBean);
    }
}
