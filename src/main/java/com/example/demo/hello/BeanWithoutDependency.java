package com.example.demo.hello;

import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/22/18.
 */
@Component
public class BeanWithoutDependency
{
    public void print()
    {
        System.out.println("Hi.... I don't have any dependency.");
    }
}
