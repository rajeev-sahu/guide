package com.example.demo.hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

/**
 * Created by rajeev.s on 5/20/18.
 */
@PropertySource("classpath:application.properties")
@Configuration
@ComponentScan
public class Application
{
    @Bean(value = "messageService")
    MessageService messageService()
    {
        return () -> "Hey! messageService!";
    }

    @Bean
    MessageService messageService2()
    {
        return () -> "Hey! messageService2";
    }

    @Bean
    MessageService messageServiceNew()
    {
        return () -> "Hey! messageServiceNew";
    }

    public static void main(String[] args) throws InterruptedException
    {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);


        KhabariLal khabariLal = context.getBean(KhabariLal.class);
        khabariLal.printMessage();
        khabariLal.printMessageWithOwnMessageService(() -> "Kya be");

        ((MessagePrinter) context.getBean("messagePrinter")).printMessage();

        context.getBean(OneBean.class).log();

        context.getBean(BeanWithoutDependency.class).print();

    }
}
