package com.example.demo.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/20/18.
 */
@Component
public class MessagePrinter
{
    private MessageService messageService;

    private MessagePrinter()
    {
        System.out.println("MessagePrinter: CONSTRUCTOR");
    }

    @Required
    @Autowired
    public void setMessageService(MessageService messageService)
    {
        this.messageService = messageService;
    }

    public void printMessage()
    {
        System.out.println("MessagePrinter.messageService = " + messageService.getMessage());
    }
}
