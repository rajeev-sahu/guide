package com.example.demo.orm.persistance;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by rajeev.s on 5/29/18.
 */
@Configuration
@EnableTransactionManagement
public class ORMDatabaseConfiguration
{
    @Bean
    public HikariDataSource hikariDataSource()
    {
        HikariDataSource source = new HikariDataSource();
        source.setDriverClassName("org.postgresql.Driver");
        source.setJdbcUrl("jdbc:postgresql://localhost:5432/guide");
        source.setUsername("rajeev.s");
        source.setPassword("");
        return source;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource hikariDataSource)
    {
        final LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(hikariDataSource);
        sessionFactoryBean.setPackagesToScan("com.example.demo.orm", "com.example.demo.orm.persistance");

        Properties props = new Properties();
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
//        props.setProperty("hibernate.globally_quoted_identifiers", "true");
        props.setProperty("hibernate.show_sql", "true");

        sessionFactoryBean.setHibernateProperties(props);
        return sessionFactoryBean;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory)
    {
        return new HibernateTransactionManager(sessionFactory);
    }

/*
    // To see exceptions
    @Bean
    public BeanPostProcessor persistenceTranslation()
    {
        return new PersistenceExceptionTranslationPostProcessor();
    }
*/
}
