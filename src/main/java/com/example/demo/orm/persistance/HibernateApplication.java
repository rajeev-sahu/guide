package com.example.demo.orm.persistance;

import com.example.demo.orm.domain.User;
import com.example.demo.orm.domain.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

/**
 * Created by rajeev.s on 5/29/18.
 */
@ComponentScan(basePackageClasses = {UserService.class, ORMDatabaseConfiguration.class})
public class HibernateApplication
{
    public static void main(String[] args)
    {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HibernateApplication.class);

        UserService bean = context.getBean(UserService.class);
        final List<User> users = bean.users();
        users.forEach(System.out::println);

        System.out.println("COUNT : " + bean.countAll());

        bean.save(new User("Rajeev Sahu","Male",38));
    }


}
