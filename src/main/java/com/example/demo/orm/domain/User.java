package com.example.demo.orm.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.text.MessageFormat;

/**
 * Created by rajeev.s on 5/29/18.
 */
@Entity(name = "users")
public class User
{
    private String name, gender;

    private int age;
    @Id
    private int id;

    private User()
    {
    }

    public User(String name, String gender, int age)
    {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString()
    {
        return MessageFormat.format("[ Name: {0} , Age: {1} , Gender: {2} ]", name, age, gender);
    }
}
