package com.example.demo.orm.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by rajeev.s on 5/29/18.
 */
@Component
public class UserService
{
    @Autowired
    UserRepository userRepository;

    @Transactional
    public List<User> users()
    {
        return userRepository.findAll();
    }

    @Transactional
    public long countAll()
    {
        return userRepository.count();
    }

    @Transactional
    public void save(User user)
    {
        userRepository.save(user);
    }
}
