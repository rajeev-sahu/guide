package com.example.demo.orm.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rajeev.s on 5/29/18.
 */
@Repository
public class UserRepository implements IUserRepository
{
    @Autowired
    SessionFactory sessionFactory;

    private Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }

    public long count()
    {
        return findAll().size();
    }

    @SuppressWarnings("unchecked")
    public List<User> findAll()
    {
        return (List<User>) currentSession().createCriteria(User.class).list();
    }

    public void save(final User user)
    {
        Serializable id = currentSession().save(user);

        System.out.println("ID " + id);
    }
}
