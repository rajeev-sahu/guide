package com.example.demo.internationalization.db;

import com.example.demo.internationalization.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Map;

public interface MessageRepository extends JpaRepository<Message, String>
{
/*
//    @Query("SELECT lm.message_name,t1.description,lm.message,lm.language_code " +
//            "FROM message m JOIN language_message lm " +
//            "ON t1.message_name = t2.message_name " +
//            "ORDER BY t2.message_name")

//    @Query("select message.message_name,description,message,language_code FROM message , language_message WHERE message.message_name = language_message.message_name order by message.message_name")
    public Map<String, Map<String, String>> readAll();
*/

}
