package com.example.demo.internationalization.db;

import com.example.demo.internationalization.domain.LanguageMessage;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LanguageMessageRepository extends JpaRepository<LanguageMessage, String>
{
    @Cacheable(cacheNames = "language_message")
    public List<LanguageMessage> findAll();

}
