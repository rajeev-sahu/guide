package com.example.demo.internationalization;

import com.example.demo.internationalization.domain.LanguageMessage;
import com.example.demo.internationalization.domain.MessageService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

@ComponentScan
public class InternationalizationApplication
{
    public static void main(String[] args)
    {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(InternationalizationApplication.class);

        final MessageService bean = context.getBean(MessageService.class);

//        bean.findAll().forEach(System.out::println);

        List<LanguageMessage> allData = bean.allData();

        System.out.println("allData = " + allData);

        System.out.println("\n#################### AFTER ####################\n");


        System.out.println(bean.allData());

        System.out.println("\n#################### ONCE ####################\n");

        System.out.println(bean.getMessage("add.user.save","ru"));

        System.out.println("\n#################### ONCE ####################\n");

        System.out.println(bean.getMessage("add.user.save","ru"));


    }
}
