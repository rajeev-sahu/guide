package com.example.demo.internationalization.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.text.MessageFormat;

@Entity(name = "language_message")
public class LanguageMessage
{
    @EmbeddedId
    private LanguageMessagePrimaryKey primaryKey;

    private String message;

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append(MessageFormat.format("{0} : {1} : {2}",
                                             primaryKey.getMessageName(),
                                             primaryKey.getLanguageCode(),
                                             message))
                .toString();
    }

    public String getMessageName()
    {
        return primaryKey.getMessageName();
    }

    public String getLanguageCode()
    {
        return primaryKey.getLanguageCode();
    }

    public String getMessage()
    {
        return message;
    }
}
