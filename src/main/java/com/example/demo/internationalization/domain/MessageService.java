package com.example.demo.internationalization.domain;

import com.example.demo.internationalization.db.LanguageMessageRepository;
import com.example.demo.internationalization.db.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public class MessageService
{
    @Autowired
    MessageRepository repository;

    @Autowired
    LanguageMessageRepository languageMessageRepository;

    public String getMessage(final String messageName, final String languageCode)
    {
        final List<LanguageMessage> languageMessages = allData();

        return languageMessages.stream()
                               .filter(e -> (messageName.equals(e.getMessageName()) && languageCode.equals(e.getLanguageCode())))
                               .findAny().orElseThrow(IllegalArgumentException::new).getMessage();
    }

    public List<Message> findAll()
    {
        return repository.findAll();
    }

    public List<LanguageMessage> allData()
    {
        return languageMessageRepository.findAll();
    }
}
