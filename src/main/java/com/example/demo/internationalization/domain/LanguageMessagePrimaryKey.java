package com.example.demo.internationalization.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class LanguageMessagePrimaryKey implements Serializable
{
    @Column(name = "message_name")
    private String messageName;

    @Column(name = "language_code")
    private String languageCode;

    public String getMessageName()
    {
        return messageName;
    }

    public String getLanguageCode()
    {
        return languageCode;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        LanguageMessagePrimaryKey that = (LanguageMessagePrimaryKey) o;

        return new EqualsBuilder()
                .append(messageName, that.messageName)
                .append(languageCode, that.languageCode)
                .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37)
                .append(messageName)
                .append(languageCode)
                .toHashCode();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("messageName", messageName)
                .append("languageCode", languageCode)
                .toString();
    }
}
