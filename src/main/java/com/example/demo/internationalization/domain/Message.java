package com.example.demo.internationalization.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.text.MessageFormat;

@Entity(name = "message")
public class Message
{
    @Column(name = "message_name")
    @Id
    String messageName;

    String description;

    @Override
    public String toString()
    {
        return MessageFormat.format("{0} : {1}", messageName, description);
    }
}
