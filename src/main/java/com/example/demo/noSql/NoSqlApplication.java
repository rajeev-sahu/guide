package com.example.demo.noSql;

import com.example.demo.noSql.domain.Customer;
import com.example.demo.noSql.domain.Item;
import com.example.demo.noSql.domain.Order;
import com.example.demo.noSql.domain.OrderService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by rajeev.s on 6/1/18.
 */
@ComponentScan
public class NoSqlApplication
{
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NoSqlApplication.class);

        final OrderService orderService = context.getBean(OrderService.class);

//        saveRecord(orderService);

//        List<Order> order = orderService.find();
//
//        System.out.println(order);

        System.out.println("BEFORE DELETION: " + orderService.count());

        orderService.delete("5b110c0d649e6b3cd4519207");

        System.out.println("AFTER DELETION: " + orderService.count());
    }

    private static void saveRecord(OrderService orderService)
    {
        Customer customer = new Customer();
        customer.setName("Rajeev Sahu");
        customer.setAddress("C-203, Dharraj Valley , Goregaon East");
        customer.setMobileNo("9867138008");
        customer.setCustomerId(3);

        final List<Item> items = new ArrayList<>();

        final double basePrice = 10.23;
        IntStream.rangeClosed(1, 3).forEach(i ->
                                            {
                                                Item item = new Item();
                                                item.setId(i);
                                                item.setPrice(basePrice * i);
                                                item.setDiscountPercentage(i);
                                                item.setQuantity(i * 2);
                                                items.add(item);
                                            });

        final Order order = new Order();

        order.setCustomer(customer);
        order.setItems(items);

        orderService.save(order);
    }
}
