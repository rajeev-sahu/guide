package com.example.demo.noSql;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by rajeev.s on 6/1/18.
 */
@Configuration
@EnableMongoRepositories("com.example.demo.noSql.db")
public class NoSqlDatabaseConfig extends AbstractMongoConfiguration
{
    @Override
    protected String getDatabaseName()
    {
        return "springdata";
    }

    @Override
    public MongoClient mongoClient()
    {
        return new MongoClient();
    }
}
