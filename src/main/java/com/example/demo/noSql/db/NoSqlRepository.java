package com.example.demo.noSql.db;

import com.example.demo.noSql.domain.Customer;
import com.example.demo.noSql.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rajeev.s on 6/1/18.
 */
@Repository
public class NoSqlRepository
{
    @Autowired
    MongoOperations mongoOperations;

    public void save(Order order)
    {
        mongoOperations.save(order);
    }

    public Order findOne(final String orderId)
    {
        return mongoOperations.findById(orderId, Order.class);
    }

    public List<Order>  find()
    {
        return mongoOperations.find(Query.query(Criteria.where("customer.id").is(2)),
                                    Order.class);
    }
}
