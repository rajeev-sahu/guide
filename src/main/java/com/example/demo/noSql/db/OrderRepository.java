package com.example.demo.noSql.db;

import com.example.demo.noSql.domain.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, String>
{
}
