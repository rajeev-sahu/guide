package com.example.demo.noSql.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by rajeev.s on 6/1/18.
 */
public class Item
{
    int id;
    double price;
    int quantity;
    double discountPercentage;

    public void setId(int id)
    {
        this.id = id;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public void setDiscountPercentage(double discountPercentage)
    {
        this.discountPercentage = discountPercentage;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("price", price)
                .append("quantity", quantity)
                .append("discountPercentage", discountPercentage)
                .toString();
    }
}
