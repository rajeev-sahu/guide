package com.example.demo.noSql.domain;

import com.example.demo.noSql.db.NoSqlRepository;
import com.example.demo.noSql.db.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by rajeev.s on 6/1/18.
 */
@Component
public class OrderService
{
    @Autowired
    NoSqlRepository mongoRepository;

    @Autowired
    OrderRepository orderRepository;

    public void save(Order order)
    {
        mongoRepository.save(order);
    }

    public Order findByOrderId(final String orderId)
    {
        return mongoRepository.findOne(orderId);
    }

    public List<Order> find()
    {
        return mongoRepository.find();
    }

    public long count()
    {
        return orderRepository.count();
    }

    public void delete(final String id)
    {
        orderRepository.deleteById(id);
    }

}
