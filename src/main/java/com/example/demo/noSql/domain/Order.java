package com.example.demo.noSql.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rajeev.s on 6/1/18.
 */
@Document
public class Order
{
    @Id
    String id;

    @Field("customer")
    Customer customer;

    @Field("lineItems")
    List<Item> items;

    public void setCustomer(final Customer customer)
    {
        this.customer = customer;
    }

    public void setItems(final List<Item> items)
    {
        this.items = new ArrayList<>(items);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("customer", customer)
                .append("items", items)
                .toString();
    }
}
