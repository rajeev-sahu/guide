package com.example.demo.noSql.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by rajeev.s on 6/1/18.
 */
public class Customer
{
    @Field("id")
    int customerId;
    String name;
    String address;
    String mobileNo;

    public void setCustomerId(int customerId)
    {
        this.customerId = customerId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void setMobileNo(String mobileNo)
    {
        this.mobileNo = mobileNo;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("customerId", customerId)
                .append("name", name)
                .append("address", address)
                .append("mobileNo", mobileNo)
                .toString();
    }
}
