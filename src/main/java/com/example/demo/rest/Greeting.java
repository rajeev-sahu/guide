package com.example.demo.rest;

import java.text.MessageFormat;

public class Greeting
{
    private long id;

    private String name;

    public Greeting(long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public String sayHello()
    {
        return MessageFormat.format("{0}: Hello {1} !!", id, name);
    }
}
