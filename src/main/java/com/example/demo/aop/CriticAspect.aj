package com.example.demo.aop;

import com.example.demo.concert.CriticismEngine;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/27/18.
 */
@Component
public aspect CriticAspect
{
    private CriticismEngine criticismEngine;

    public CriticAspect()
    {

    }

    public void setCriticismEngine(CriticismEngine criticismEngine)
    {
        this.criticismEngine = criticismEngine;
    }

    pointcut performance(): execution(* com.example.demo.concert.Performance.perform(..));

//    afterReturning(): performance()
//    {
//        System.out.println(criticismEngine.getCriticism());
//    }


}
