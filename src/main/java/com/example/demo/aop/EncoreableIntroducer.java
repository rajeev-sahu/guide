package com.example.demo.aop;

import com.example.demo.concert.Encoreable;
import com.example.demo.concert.EncoreableImplementation;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/26/18.
 */
@Aspect
@Component
public class EncoreableIntroducer
{
    @DeclareParents(value = "com.example.demo.concert.Performance+",
            defaultImpl = EncoreableImplementation.class)
    public static Encoreable encoreable;

}
