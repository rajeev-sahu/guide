package com.example.demo.aop;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by rajeev.s on 5/26/18.
 */
@Aspect
@Component
public class Audience
{
    final Map<String, Integer> mapping = new HashMap<>();

    @Pointcut(value = "execution(* com.example.demo.concert.Performance.perform(..))")
    private void performance()
    {

    }

    @Around("performance()")
    public void around(ProceedingJoinPoint joinPoint)
    {
        System.out.println("AROUND: Audience have put their phones on SILENCE");
        System.out.println("AROUND: Audience is now settling down");
        Object[] args = joinPoint.getArgs();
        try
        {

            System.out.println(joinPoint.getKind());
            System.out.println(Arrays.toString(args));

            mapping.put(String.valueOf(args[0]), (mapping.getOrDefault(String.valueOf(args[0]), 0) + 1));

            joinPoint.proceed(new String[]{"Mr. " + StringUtils.capitalize(String.valueOf(args[0]))});
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            System.out.println("AROUND: Refund");
        }
        System.out.println("AROUND: CLAP... CLAP... CLAP..." + mapping.get(String.valueOf(args[0])));

    }

    /*

        @Before("performance()")
        public void takeSeat()
        {
            System.out.println("Audience is now settling down");
        }

        @Before("performance()")
        public void silentPhone()
        {
            System.out.println("Audience have put their phones on SILENCE");
        }

        @After("performance()")
        public void clap()
        {
            System.out.println("CLAP... CLAP... CLAP...");
        }

    */
    @AfterReturning("performance()")
    public void afterReturning()
    {
        System.out.println("Completed Successfully...");
    }

}
