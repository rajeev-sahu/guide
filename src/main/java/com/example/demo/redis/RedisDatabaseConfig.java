package com.example.demo.redis;

import com.example.demo.redis.domain.ProductInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Configuration
@EnableRedisRepositories(basePackages = "com.example.demo.redis.db")
public class RedisDatabaseConfig
{
    @Bean
    public JedisConnectionFactory redisConnectionFactory()
    {
        return new JedisConnectionFactory();
    }

    @Bean
    public RedisTemplate<String, ProductInfo> redisTemplate()
    {
        final RedisTemplate<String, ProductInfo> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory());
//        redisTemplate.setValueSerializer(new GenericToStringSerializer<>(ProductInfo.class));
        return redisTemplate;
    }
}
