package com.example.demo.redis;

import com.example.demo.redis.domain.ProductInfo;
import com.example.demo.redis.domain.RedisService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class RedisApplication
{
    public static void main(String[] args)
    {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(RedisApplication.class);
        RedisService bean = context.getBean(RedisService.class);

        final ProductInfo productInfo = getProductInfo();

        bean.save(productInfo);


//        bean.delete(productInfo.getProductKey());
        System.out.println(bean.getProductInfo("mojo"));
        System.out.println(bean.getProductInfo("weebly"));
        System.out.println(bean.getProductInfo("bhag"));

    }

    private static ProductInfo getProductInfo()
    {
        final ProductInfo productInfo = new ProductInfo();
        productInfo.setProductKey("weebly");
        productInfo.setTenures(new int[]{12,24});
        productInfo.setPricingModel(ProductInfo.PricingModel.PLAN_ADDON);
        return productInfo;
    }
}
