package com.example.demo.redis.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.util.Arrays;

@RedisHash("ProductInfo")
public class ProductInfo
{
    @Id
    private String productKey;
    private int[] tenures;
    private PricingModel pricingModel;

    public String getProductKey()
    {
        return productKey;
    }

    public void setProductKey(String productKey)
    {
        this.productKey = productKey;
    }

    public void setTenures(int[] tenures)
    {
        this.tenures = Arrays.copyOf(tenures, tenures.length);
    }

    public void setPricingModel(PricingModel pricingModel)
    {
        this.pricingModel = pricingModel;
    }

    public enum PricingModel
    {
        GLOBAL_ADDON,
        PLAN_ADDON,
        BAND_BASED;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("productKey", productKey)
                .append("tenures", tenures)
                .append("pricingModel", pricingModel)
                .toString();
    }
}
