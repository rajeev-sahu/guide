package com.example.demo.redis.domain;

import com.example.demo.redis.db.ProductInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RedisService
{
    @Autowired
    private ProductInfoRepository repository;

    public void save(final ProductInfo productInfo)
    {
        repository.save(productInfo);
    }

    public ProductInfo getProductInfo(final String productKey)
    {
        return repository.findById(productKey).orElse(null);
        //HGETALL ProductInfo:mojo
    }

    public void delete(final String productKey)
    {
        repository.deleteById(productKey);
    }
}
