package com.example.demo.redis.db;

import com.example.demo.redis.domain.ProductInfo;
import org.springframework.data.repository.CrudRepository;

public interface ProductInfoRepository extends CrudRepository<ProductInfo, String>
{
}
