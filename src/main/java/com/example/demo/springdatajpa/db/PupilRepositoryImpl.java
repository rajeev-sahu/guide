package com.example.demo.springdatajpa.db;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by rajeev.s on 5/31/18.
 */
public class PupilRepositoryImpl
{
    @PersistenceContext
    EntityManager entityManager;

    public void update()
    {
        entityManager.createQuery("UPDATE pupils SET category = 'GENERAL' WHERE id between 1 and 5").executeUpdate();
    }
}
