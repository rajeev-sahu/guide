package com.example.demo.springdatajpa.db;


import com.example.demo.springdatajpa.domain.Student;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by rajeev.s on 5/31/18.
 */
public interface StudentRepository extends Repository<Student, Long>
{
    List<Student> findByName(final String name);

    List<Student> findByFatherName(final String fatherName);

    List<Student> getDistinctStudentByFatherName(final String fatherName);

    List<Student> getStudentsByIdGreaterThanEqual(final int id);

    List<Student> readByNameIgnoringCase(final String name);

    void save(final Student student);

}
