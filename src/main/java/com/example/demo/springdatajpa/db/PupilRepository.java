package com.example.demo.springdatajpa.db;

import com.example.demo.springdatajpa.domain.Pupil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by rajeev.s on 5/31/18.
 */
public interface PupilRepository extends Repository<Pupil, Integer>
{
    List<Pupil> findPupilByFirstNameOrderByLastName(final String firstName);
    void save(final Pupil pupil);

    List<Pupil> readPupilsByCategoryOrderByLastNameDesc(Pupil.Category category);


    @Query("select p from pupils p order by category desc")
    public List<Pupil> readAll();

    public void update();



}
