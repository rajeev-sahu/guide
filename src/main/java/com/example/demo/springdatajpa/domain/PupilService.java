package com.example.demo.springdatajpa.domain;

import com.example.demo.springdatajpa.db.PupilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by rajeev.s on 5/31/18.
 */
@Component
@Transactional
public class PupilService
{
    @Autowired
    PupilRepository pupilRepository;

    public void printPupilOrderByLastName(final String firstName)
    {
        pupilRepository.findPupilByFirstNameOrderByLastName(firstName).forEach(System.out::println);
    }

    public void save(Pupil pupil)
    {
        pupilRepository.save(pupil);
    }

    public List<Pupil> readPupilsByCategoryOrderByLastNameDesc()
    {
        return pupilRepository.readAll();
//        return pupilRepository.readPupilsByCategoryOrderByLastNameDesc(Pupil.Category.OBC);
    }

    public void update()
    {
        pupilRepository.update();
    }



}
