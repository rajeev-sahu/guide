package com.example.demo.springdatajpa.domain;

import com.example.demo.springdatajpa.db.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rajeev.s on 5/31/18.
 */
@Component
@Transactional
public class StudentService
{
    @Autowired
    StudentRepository studentRepository;

    public List<Student> findByName(final String name)
    {
        return studentRepository.readByNameIgnoringCase(name);
    }

    public List<Student> findByFatherName(final String fatherName)
    {
        return studentRepository.findByFatherName(fatherName);
    }

    public List<Student> getAllStudents(int id)
    {
        return studentRepository.getStudentsByIdGreaterThanEqual(id);
    }

    public List<String> getDistinctStudentByFatherName(final String fatherName)
    {
        List<Student> distinctStudentByFatherName = studentRepository.getDistinctStudentByFatherName(fatherName);

        return distinctStudentByFatherName.stream().map(e -> e.name).collect(Collectors.toList());

    }

    public void insert(final Student student)
    {
        studentRepository.save(student);
    }
}
