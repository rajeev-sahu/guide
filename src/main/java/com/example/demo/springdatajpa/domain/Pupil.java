package com.example.demo.springdatajpa.domain;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;

/**
 * Created by rajeev.s on 5/31/18.
 */
@Entity(name = "pupils")
public class Pupil
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(name = "first_name")
    String firstName;
    @Column(name = "last_name")
    String lastName;
    String course;
    @Column(name = "father_name")
    String fatherName;

    String gender;

    @Enumerated(EnumType.STRING)
    Category category;

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setCourse(String course)
    {
        this.course = course;
    }

    public void setFatherName(String fatherName)
    {
        this.fatherName = fatherName;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public void setCategory(Category category)
    {
        this.category = category;
    }

    public enum Category
    {
        GENERAL, OBC, SC, ST
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("id", id)
                .append("firstName", firstName)
                .append("lastName", lastName)
                .append("course", course)
                .append("fatherName", fatherName)
                .append("gender", gender)
                .append("category", category)
                .toString();
    }
}
