package com.example.demo.springdatajpa;

import com.example.demo.springdatajpa.domain.Pupil;
import com.example.demo.springdatajpa.domain.PupilService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

/**
 * Created by rajeev.s on 5/31/18.
 */
@ComponentScan
public class SpringDataJpaApplication
{
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringDataJpaApplication.class);

/*
        Student student = new Student();
        student.setId(3);
        student.setName("Rajeev Sahu");
        student.setFatherName("Shri Jagannath Sahu");
        student.setCourse("MCA");
*/

//        StudentService studentService = context.getBean(StudentService.class);

//        List<Student> students = studentService.findByFatherName("Shri Motilal Sahu");
//
//        System.out.println("students = " + students);

//        System.out.println(studentService.getDistinctStudentByFatherName("Shri Jagannath Sahu"));
//        System.out.println(studentService.getAllStudents(2));
//        System.out.println(studentService.findByName("rajeev sahu"));

/*


        studentService.insert(student);

        students = studentService.findByName("Rajeev Sahu");

        System.out.println("After INSERT = " + students);
*/


        PupilService pupilService = context.getBean(PupilService.class);

/*
        IntStream.rangeClosed(21, 21).forEach(i ->
                                              {
                                                  Pupil pupil = new Pupil();
                                                  pupil.setFirstName("FirstName " + i);
                                                  pupil.setLastName("LastName " + i);
                                                  pupil.setFatherName("FatherName " + i);
                                                  pupil.setCourse("MBA" + i);
                                                  pupil.setGender("FEMALE");
                                                  pupil.setCategory(Pupil.Category.ST);

                                                  pupilService.save(pupil);
                                              });
*/


//        List<Pupil> pupils = pupilService.readPupilsByCategoryOrderByLastNameDesc();
//
//        pupils.forEach(System.out::println);


        pupilService.update();


    }
}
