package com.example.demo.music;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/22/18.
 */
@Component
public class CDPlayer
{
    private ICompactDisk compactDisk;

    //    @Autowired
//    public CDPlayer()
//    {
//        this.compactDisk = compactDisk;
//        System.out.println("Was in CDPlayer " + compactDisk.getClass().getName());
//    }
    @Autowired
    @Qualifier("favourite")
    public void setCompactDisk(ICompactDisk compactDisk)
    {
        this.compactDisk = compactDisk;
    }

    public void playMusic()
    {
        compactDisk.play();
    }
}
