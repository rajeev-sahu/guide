package com.example.demo.music;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by rajeev.s on 5/22/18.
 */
@Configuration
@ComponentScan
public class MusicApplication
{
/*
    @Bean
    @Conditional(ConditionImplementation.class)
    public ICompactDisk lambda()
    {
        return () -> System.out.println("Bhai!!! lambda.com");
    }
*/

/*

    @Bean
    public ICompactDisk ganna()
    {
        return new Ganna();
    }

    @Bean
    @Qualifier("favourite")
    public ICompactDisk rockSong()
    {
        return new RockSong();
    }

    @Bean
//    @Primary
    public ICompactDisk lambda()
    {
        return () -> System.out.println("Bhai!!! lambda.com");
    }
*/


/*
    @Bean("rockSongCD")
    public CDPlayer cdPlayerGanna()
    {
        return new CDPlayer(rockSong());
    }

    @Bean("gannaCD")
    public CDPlayer cdPlayerRockSong()
    {
        return new CDPlayer(ganna());
    }

    @Bean("lambdaCD")
    public CDPlayer cdPlayerLambda()
    {
        return new CDPlayer(lambda());
    }
*/


    public static void main(String[] args)
    {
        ApplicationContext context = new AnnotationConfigApplicationContext(MusicApplication.class);

//        ((CDPlayer) context.getBean(args[Integer.parseInt(args[3])])).playMusic();
        context.getBean(CDPlayer.class).playMusic();

/*
        context.getBean(CDPlayer.class).playMusic();

        Object lambda = context.getBean("lambda");

        System.out.println("lambda = " + lambda);
*/
        final Set<TextMate> set = new HashSet<>();

        for (int i = 0; i <= 3; i++)
        {
            set.add(context.getBean(TextMate.class));
        }

        System.out.println("set.size(): " + set.size());

        set.stream().forEach(System.out::println);


    }

//    @Bean
//    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer()
//    {
//        return new PropertySourcesPlaceholderConfigurer();
//    }
//
    private static void printEnvironmentVariables()
    {
        final Map<String, String> environmentVariables = System.getenv();

        environmentVariables.keySet().stream().forEach((String envVariableName) ->
                                                               System.out.println(MessageFormat.format("{0} >> {1}",
                                                                                                       envVariableName,
                                                                                                       environmentVariables
                                                                                                               .get(envVariableName))));
    }
}
