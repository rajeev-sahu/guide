package com.example.demo.music;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Arrays;

/**
 * Created by rajeev.s on 5/23/18.
 */
public class ConditionImplementation implements Condition
{
    public final static Condition CONDITION_USER = (ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) ->
    {
        String[] activeProfiles = conditionContext.getEnvironment().getActiveProfiles();
        System.out.println("activeProfiles = " + Arrays.toString(activeProfiles));

        return "rajeev.s".equals(conditionContext.getEnvironment().getProperty("USER"));
    };

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata)
    {
        return CONDITION_USER.matches(conditionContext, annotatedTypeMetadata);
    }
}
