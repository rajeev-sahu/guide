package com.example.demo.music;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by rajeev.s on 5/25/18.
 */
@Component
//@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@PropertySource("classpath:application.properties")
public class TextMate
{
    private final String name;

    private final String lines;

    private ICompactDisk compactDisk;

    public TextMate(@Value("#{systemProperties['user.country']}") final String name,
                    @Value("${lines}") final String lines,
                    @Value("#{rockSong}") final ICompactDisk compactDisk)
    {
        this.name = Objects.requireNonNull(name, "Empty: name");

        this.lines = Objects.requireNonNull(lines, "Invalid: lines");

        this.compactDisk = compactDisk;

    }

    @Override
    public String toString()
    {
        compactDisk.play();
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("name", name)
                .append("lines", lines)
                .toString();
    }
}
