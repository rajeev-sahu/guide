package com.example.demo.music;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/22/18.
 */
@Component
public class RockSong implements ICompactDisk
{
    @Override
    public void play()
    {
        System.out.println("I'm rocksong.com");
    }
}
