package com.example.demo.jpa.domain;

import com.example.demo.jpa.persistence.JpaStudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/30/18.
 */
@Component
public class StudentService
{
    @Autowired
    JpaStudentRepository repository;

    public void addStudent(Student student)
    {
        repository.addStudent(student);
    }

    public Student getStudentById(int id)
    {
        return repository.getStudentById(id);

    }
}
