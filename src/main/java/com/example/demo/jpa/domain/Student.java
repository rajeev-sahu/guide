package com.example.demo.jpa.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.MessageFormat;

/**
 * Created by rajeev.s on 5/30/18.
 */
@Entity
@Table(name = "students")
public class Student
{
    @Id
    int id;

    String name;

    @Column(name = "fathername")
    String fatherName;

    String course;

    public Student()
    {
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setFatherName(String fatherName)
    {
        this.fatherName = fatherName;
    }

    public void setCourse(String course)
    {
        this.course = course;
    }

    @Override
    public String toString()
    {
        return MessageFormat.format("[ID: {0} , NAME: {1} , FATHER NAME: {2} , COURSE: {3}]", id, name, fatherName, course);
    }
}
