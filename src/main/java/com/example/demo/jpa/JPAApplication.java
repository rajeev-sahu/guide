package com.example.demo.jpa;

import com.example.demo.jpa.domain.Student;
import com.example.demo.jpa.domain.StudentService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by rajeev.s on 5/30/18.
 */
@ComponentScan
public class JPAApplication
{
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JPAApplication.class);

        final StudentService studentService = context.getBean(StudentService.class);
        Student student = new Student();
        student.setId(2);
        student.setName("Sanjeev Sahu");
        student.setFatherName("Jagannath Sahu");
        student.setCourse("B.Tech");
        studentService.addStudent(student);

        Student studentById = studentService.getStudentById(1);

        System.out.println("studentById = " + studentById);
    }
}
