package com.example.demo.jpa.persistence;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by rajeev.s on 5/30/18.
 */
@Configuration
@ComponentScan(basePackages = {"com.example.demo.jpa.domain", "com.example.demo.jpa.persistence"})
@EnableTransactionManagement
public class JPAConfiguration
{
    @Bean
    public HikariDataSource hikariDataSource()
    {
        HikariDataSource source = new HikariDataSource();
        source.setDriverClassName("org.postgresql.Driver");
        source.setJdbcUrl("jdbc:postgresql://localhost:5432/guide");
        source.setUsername("rajeev.s");
        source.setPassword("");
        return source;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter()
    {
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

        vendorAdapter.setDatabase(Database.POSTGRESQL);
        vendorAdapter.setShowSql(true);
        vendorAdapter.setGenerateDdl(false);
        vendorAdapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");

        System.out.println("JPA Vendor");
        return vendorAdapter;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource hikariDataSource,
                                                                       JpaVendorAdapter jpaVendorAdapter)
    {
        final LocalContainerEntityManagerFactoryBean managerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        managerFactoryBean.setDataSource(hikariDataSource);
        managerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        managerFactoryBean.setPackagesToScan("com.example.demo.jpa.domain"); // this package will be scanned for class annotated with @Entity

        return managerFactoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager()
    {
        return new JpaTransactionManager();
    }

/*
    @Bean
    public PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor()
    {
        return new PersistenceAnnotationBeanPostProcessor();
    }


    @Bean
    public BeanPostProcessor persistenceTranslation()
    {
        return new PersistenceExceptionTranslationPostProcessor();
    }

*/
}
