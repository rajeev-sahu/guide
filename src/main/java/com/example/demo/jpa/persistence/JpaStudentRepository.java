package com.example.demo.jpa.persistence;

import com.example.demo.jpa.domain.Student;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


/**
 * Created by rajeev.s on 5/30/18.
 */
@Repository
@Transactional
public class JpaStudentRepository
{
    @PersistenceContext
    EntityManager entityManager;


    public void addStudent(final Student student)
    {
        entityManager.persist(student);
    }

    public Student getStudentById(int id)
    {
        return entityManager.find(Student.class, id);
    }
}
