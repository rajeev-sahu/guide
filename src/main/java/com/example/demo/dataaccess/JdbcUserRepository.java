package com.example.demo.dataaccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

/**
 * Created by rajeev.s on 5/27/18.
 */
@Repository
public class JdbcUserRepository implements UserRepository
{
    @Autowired
    private JdbcOperations jdbcOperations;

    private static final String INSERT_QUERY = "INSERT INTO users (name,age,gender) VALUES (?,?,?)";

    private static final String SELECT_QUERY = "SELECT * FROM users LIMIT 1";


    public void addUser(final User user)
    {
        jdbcOperations.update(INSERT_QUERY, user.getName(), user.getAge(), user.getGender());
    }

    public User getUser()
    {
        return jdbcOperations.queryForObject(SELECT_QUERY,
                                             (resultSet, i) ->
                                             {
                                                 return new User(resultSet.getString("name"),
                                                                 resultSet.getString("age"),
                                                                 resultSet.getString("gender"));
                                             });
    }
}
