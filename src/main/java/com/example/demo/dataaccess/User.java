package com.example.demo.dataaccess;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/27/18.
 */
@Component
@PropertySource("classpath:application.properties")
public class User
{
    private String name;
    private int age;
    private String gender;

    public User(@Value("${user.user_name}") String name, @Value("${user.age}") String age, @Value("${user.gender}") String gender)
    {
        this.name = name;
        this.age = Integer.parseInt(age);
        this.gender = gender;
    }

    public String getName()
    {
        return name;
    }

    public int getAge()
    {
        return age;
    }

    public String getGender()
    {
        return gender;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("name", name)
                .append("age", age)
                .append("gender", gender)
                .toString();
    }
}
