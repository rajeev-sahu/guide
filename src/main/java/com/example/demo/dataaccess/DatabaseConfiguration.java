package com.example.demo.dataaccess;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jndi.JndiObjectFactoryBean;

import javax.sql.DataSource;

/**
 * Created by rajeev.s on 5/27/18.
 */
@Configuration
@ComponentScan
public class DatabaseConfiguration
{
/*
    @Bean
    @Conditional(DataBaseSourceCondition.class)
    public JndiObjectFactoryBean jndiDataSource()
    {
        JndiObjectFactoryBean jndiObjectFB = new JndiObjectFactoryBean();
        jndiObjectFB.setJndiName("jdbc/SpittrDS");
        jndiObjectFB.setResourceRef(true);
        jndiObjectFB.setProxyInterface(javax.sql.DataSource.class);
        return jndiObjectFB;
    }

    @Bean
    @Conditional(DataBaseSourceCondition.class)
    public DataSource jdbcDataSource()
    {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/guide");
        dataSource.setUsername("rajeev.s");
        dataSource.setPassword("");
        return dataSource;
    }


    @Bean
    @Conditional(DataBaseSourceCondition.class)
    public DataSource dataSource()
    {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:schema.sql")
                .addScript("classpath:test-data.sql")
                .build();
    }

*/

    @Bean
    public HikariDataSource hikariDataSource()
    {
        HikariDataSource source = new HikariDataSource();
        source.setDriverClassName("org.postgresql.Driver");
        source.setJdbcUrl("jdbc:postgresql://localhost:5432/guide");
        source.setUsername("rajeev.s");
        source.setPassword("");
        return source;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource)
    {
        return new JdbcTemplate(dataSource);
    }
}
