package com.example.demo.dataaccess;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Created by rajeev.s on 5/27/18.
 */
@PropertySource("classpath:application.properties")
public class DataBaseSourceCondition implements Condition
{
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata)
    {
        final String userName = conditionContext.getEnvironment().getProperty("db.profile");

        System.out.println("USER NAME : " + userName);

        return "rajeev.s".equals(userName);
    }
}
