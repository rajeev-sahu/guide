package com.example.demo.dataaccess;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rajeev.s on 5/27/18.
 */
@ComponentScan
public class DatabaseApplication
{
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DatabaseApplication.class);

        UserService userService = context.getBean(UserService.class);

        userService.addUser();
//        userService.getUser();


    }
}
