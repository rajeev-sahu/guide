package com.example.demo.dataaccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/27/18.
 */
@Component
public class UserService
{
    @Autowired
    User user;

    @Autowired
    JdbcUserRepository repository;

//    public UserService(User user, JdbcUserRepository repository)
//    {
//        this.user = user;
//        this.repository = repository;
//    }
//
    public void addUser()
    {
        repository.addUser(user);
    }

    public void getUser()
    {
        System.out.println("user = " + repository.getUser());

    }
}
