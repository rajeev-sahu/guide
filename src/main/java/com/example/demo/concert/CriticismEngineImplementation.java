package com.example.demo.concert;

import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/27/18.
 */
@Component
public class CriticismEngineImplementation implements CriticismEngine
{
    @Override
    public String getCriticism()
    {
        return "It was an awesome performance.";
    }
}
