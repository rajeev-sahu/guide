package com.example.demo.concert;

//import com.example.demo.aop.CriticAspect;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

/**
 * Created by rajeev.s on 5/26/18.
 */
@Configuration
@ComponentScan(basePackages = {"com.example.demo.aop", "com.example.demo.concert"})
@EnableAspectJAutoProxy
public class PerformanceApplication
{
    //    @Bean
//    public Audience audience()
//    {
//        return new Audience();
//    }
//

//    @Bean
//    public CriticAspect criticismEngine()
//    {
//        return CriticAspect.aspectOf();
//    }

    public static void main(String[] args)
    {
        ApplicationContext context = new AnnotationConfigApplicationContext(PerformanceApplication.class);

        Performance performance = context.getBean(Performance.class);

        performance.perform("rajeev sahu");

        ((Encoreable) performance).encore();

/*
        System.out.println("######################");
        Performance performance = (Performance)context.getBean("performanceImplementation");

        performance.perform("sanjeev sahu");
*/

    }
}
