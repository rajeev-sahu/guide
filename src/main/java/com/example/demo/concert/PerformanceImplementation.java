package com.example.demo.concert;

import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/**
 * Created by rajeev.s on 5/26/18.
 */
@Component
public class PerformanceImplementation implements Performance
{
    @Override
    public void perform(final String performerName)
    {
        System.out.println(MessageFormat.format("{0} : is performing", performerName));
    }
}
