package com.example.demo.concert;

/**
 * Created by rajeev.s on 5/27/18.
 */
public interface CriticismEngine
{
    String getCriticism();
}
