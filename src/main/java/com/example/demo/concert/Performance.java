package com.example.demo.concert;

/**
 * Created by rajeev.s on 5/26/18.
 */
public interface Performance
{
    void perform(final String performerName);
}
