package com.example.demo.concert;

import org.springframework.stereotype.Component;

/**
 * Created by rajeev.s on 5/26/18.
 */
@Component
public class EncoreableImplementation implements Encoreable
{
    @Override
    public void encore()
    {
        System.out.println("EncoreableImplementation.encore");
    }
}
